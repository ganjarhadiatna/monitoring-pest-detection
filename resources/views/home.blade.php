@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">Data</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($data))
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Status Pest Detection</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                @foreach($data as $dt)
                                    @if (isset($dt['Status']))
                                        <tr>
                                            <th scope="row">{{ $i }}</th>
                                            <td>{{ $dt["Status"] }}</td>
                                            <td>{{ date("d-m-Y h:i:s A") }}</td>
                                        </tr>
                                        <?php $i++ ?>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        {{ 'Fatching data failed.' }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
