<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;  
use Kreait\Firebase\Factory;  
use Kreait\Firebase\ServiceAccount;  
use Kreait\Firebase\Database;  

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $serviceAccount = public_path('/json/service-account.json');
        $factory = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://sidekma-1b98a.firebaseio.com/');
        $database = $factory->createDatabase();
        $reference = $database->getReference('sidekma-1b98a/data');
        $snapshot = $reference->orderByKey()->limitToLast(20)->getSnapshot();
        $value = $snapshot->getValue();
        $haveChild = $snapshot->hasChildren();

        // $payload = [];
        // foreach ($value as $data) {
        //     // array_push($payload, $data['Status']);
        //     if (isset($data['Status'])) {
        //         echo json_encode($data);
        //     } else {
        //         echo "status salah";
        //     }
        // }
        // echo json_encode($payload, true);

        return view('home', ['data' => $value]);
    }
}
